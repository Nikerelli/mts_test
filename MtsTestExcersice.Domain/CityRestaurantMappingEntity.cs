﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MtsTestExcersice.Domain
{
    public class CityRestaurantMappingEntity
    {
        public int CityId { get; set; }
        public City City { get; set; }
        public int RestaurantId { get; set; }
        public Restaurant Restaurant { get; set; }
    }
}
