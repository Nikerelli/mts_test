﻿using System.Collections.Generic;

namespace MtsTestExcersice.Domain
{
    public class Restaurant : EntityBase
    {
        public string Name { get; set; }
        public List<CityRestaurantMappingEntity> Cities { get; set; }
    }
}
