﻿using System.Collections.Generic;

namespace MtsTestExcersice.Domain
{
    public class City : EntityBase
    {
        public string Name { get; set; }
        public List<CityRestaurantMappingEntity> Restaurants { get; set; }
    }
}
