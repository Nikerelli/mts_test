﻿using AutoMapper;
using CodeJam.Collections;
using Microsoft.AspNetCore.Mvc;
using MtsExcersice.WebApi.Dto.Criterions;
using MtsExcersice.WebApi.Dto.Models;
using MtsTestExcersice.Business.Abstractions;
using MtsTestExcersice.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MtsExcersice.WebApi.Controllers
{
    /// <summary/>
    [ApiController]
    [Route("api/[controller]")]
    public class RestaurantController : ControllerBase
    {
        private readonly IOperationService<Restaurant> _restaurantService;
        private readonly IMapper _mapper;

        /// <summary/>
        public RestaurantController(IOperationService<Restaurant> restaurantService, IMapper mapper)
        {
            _restaurantService = restaurantService;
            _mapper = mapper;
        }

        /// <summary>
        ///     Return all restaurants
        /// </summary>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetRestaurantsAsync([FromQuery] PagingCriterion criterion, int? cityId)
        {
            var restaurants = await _restaurantService.GetAllAsync();

            if (cityId.HasValue)
            {
                restaurants = restaurants.Where(r => r.CityIds.Contains(cityId.Value)).ToList();
            }

            var pagedDto = new GenericPagedCollection<RestaurantDto>
            {
                TotalCount = restaurants.Count,
                CurrentPage = criterion.Page == 0 ? 1 : criterion.Page
            };

            if (criterion.Page != 0 && criterion.Size != 0)
            {
                restaurants = restaurants.Page(criterion.Page, criterion.Size).ToList();
            }

            pagedDto.Data = _mapper.Map<List<RestaurantDto>>(restaurants);

            return Ok(pagedDto);
        }

        /// <summary>
        ///     Add new restaurant
        /// </summary>
        /// <param name="name"></param>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> CreateRestaurantAsync([FromBody] string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(nameof(name));
            }

            var restaurantModel = new Restaurant{ Name = name};
            await _restaurantService.AddAsync(restaurantModel);
            return Ok();
        }

        /// <summary>
        ///     Update existing restaurant
        /// </summary>
        [HttpPut]
        public async Task<IActionResult> UpdateRestaurantAsync([FromBody] RestaurantDto city)
        {
            var restaurantModel = _mapper.Map<Restaurant>(city);
            await _restaurantService.UpdateAsync(restaurantModel);
            return Ok();
        }

        /// <summary>
        ///     Remove existing restaurant
        /// </summary>
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> RemoveRestaurantAsync([FromRoute] string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException(nameof(id));
            }

            await _restaurantService.RemoveAsync(id);
            return Ok();
        }
    }
}
