﻿using AutoMapper;
using CodeJam.Collections;
using CodeJam.Strings;
using Microsoft.AspNetCore.Mvc;
using MtsExcersice.WebApi.Dto.Criterions;
using MtsTestExcersice.Business.Abstractions;
using MtsTestExcersice.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MtsExcersice.WebApi.Dto.Models;

namespace MtsExcersice.WebApi.Controllers
{
    /// <summary/>
    [ApiController]
    [Route("api/[controller]")]
    public class CityController : ControllerBase
    {
        private readonly IOperationService<City> _cityService;
        private readonly IMapper _mapper;

        /// <summary/>
        public CityController(IOperationService<City> cityService, IMapper mapper)
        {
            _cityService = cityService;
            _mapper = mapper;
        }

        /// <summary>
        ///     Return all cities
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> GetCitiesAsync([FromQuery] PagingCriterion criterion)
        {
            var cities = await _cityService.GetAllAsync();

            var pagedDto = new GenericPagedCollection<CityDto>
            {
                TotalCount = cities.Count,
                CurrentPage = criterion.Page == 0 ? 1 : criterion.Page
            };

            if (criterion.Page != 0 && criterion.Size != 0)
            {
                cities = cities.Page(criterion.Page, criterion.Size).ToList();
            }

            pagedDto.Data = _mapper.Map<List<CityDto>>(cities);

            return Ok(pagedDto);
        }

        /// <summary>
        ///     Return expanded city model by id
        /// </summary>
        [HttpGet("/{cityId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetCityAsync([FromRoute] int cityId)
        {
            var city = (await _cityService.GetAllAsync()).FirstOrDefault(x => x.Id == cityId);

            if(city == null)
            {
                return StatusCode(404, "No city has been found");
            }

            return Ok(_mapper.Map<CityFullDto>(city));
        }

        /// <summary>
        ///     Create new city
        /// </summary>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> CreateCityAsync([FromBody] string name)
        {
            if (name.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(name));
            }

            var cityModel = new City{ Name = name };
            await _cityService.AddAsync(cityModel);
            return Ok();
        }

        /// <summary>
        ///     Update existing city
        /// </summary>
        [HttpPut]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> UpdateCityAsync([FromBody] CityFullDto city)
        {
            if (city.Name.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(city.Name));
            }

            var cityModel = _mapper.Map<City>(city);
            await _cityService.UpdateAsync(cityModel);
            return Ok();
        }

        /// <summary>
        ///     Remove city by id
        /// </summary>
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> RemoveCityAsync([FromRoute] string id)
        {
            if (id.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(id));
            }

            await _cityService.RemoveAsync(id);
            return Ok();
        }
    }
}
