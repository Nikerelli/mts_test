﻿using CodeJam;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace MtsExcersice.WebApi.Middleware
{
    public class ExceptionHandler
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ExceptionHandler> _logger;

        public ExceptionHandler(RequestDelegate next, ILogger<ExceptionHandler> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next.Invoke(context);
            }
            catch (ArgumentNullException ex)
            {
                _logger.LogError(ex.ToDiagnosticString());
                context.Response.StatusCode = 400;
                await context.Response.WriteAsync("Model validation error");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToDiagnosticString());
                context.Response.StatusCode = 500;
                await context.Response.WriteAsync("Internal system error");
            }
        }
    }
}
