using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using MtsExcersice.WebApi.Configuration;
using MtsExcersice.WebApi.Mapping;
using MtsExcersice.WebApi.Middleware;
using MtsTestExcersice.Business;
using MtsTestExcersice.DataAccess.Abstractions;
using System;
using System.Reflection;

namespace MtsExcersice.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.SetupBusines();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "MTS test API",
                });

                c.IncludeXmlComments(AppContext.BaseDirectory + Assembly.GetExecutingAssembly().GetName().Name + ".xml");
            });

            var dbc = Configuration.GetConnectionString("mtsDB");
            services.AddScoped<IDataBaseConfiguration>(p => new DatabaseConfiguration { ConnectionString = dbc });

            services.AddScoped<IMapper>(p =>
                new MapperConfiguration(e => e.AddProfiles(p.GetServices<Profile>())).CreateMapper(p.GetService));
            services.AddScoped<Profile, CityMappingProfile>();
            services.AddScoped<Profile, RestaurantMappingProfile>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(s => s.SwaggerEndpoint("/swagger/v1/swagger.json", "MTS test API"));

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseMiddleware<ExceptionHandler>();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
