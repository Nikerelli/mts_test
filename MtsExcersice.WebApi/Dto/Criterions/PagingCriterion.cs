﻿namespace MtsExcersice.WebApi.Dto.Criterions
{
    /// <summary/>
    public class PagingCriterion
    {
        /// <summary>
        ///     Page number
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        ///     Page size
        /// </summary>
        public int Size { get; set; }
    }
}
