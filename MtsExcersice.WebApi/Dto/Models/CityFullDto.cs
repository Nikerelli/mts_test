﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MtsExcersice.WebApi.Dto.Models
{
    /// <inheritdoc cref="CityDto"/>
    public class CityFullDto : CityDto
    {
        /// <summary>
        ///     Restaurants that are connected with this city
        /// </summary>
        [DataMember]
        public List<RestaurantDto> Restaurants { get; set; }
    }
}
