﻿using System.Runtime.Serialization;

namespace MtsExcersice.WebApi.Dto.Models
{
    public class RestaurantDto
    {
        [DataMember]
        public int? Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
