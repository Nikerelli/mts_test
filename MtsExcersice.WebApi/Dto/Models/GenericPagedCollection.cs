﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MtsExcersice.WebApi.Dto.Models
{
    /// <summary>
    ///     Generic paginated model
    /// </summary>
    /// <typeparam name="TModel">Data type</typeparam>
    public class GenericPagedCollection<TModel>
    {
        /// <summary>
        ///     Total available count of data objects
        /// </summary>
        [DataMember]
        public int TotalCount { get; set; }

        /// <summary>
        ///     Current page number
        /// </summary>
        [DataMember]
        public int CurrentPage { get; set; }

        /// <summary>
        ///     Objects that are at the defined page
        /// </summary>
        [DataMember]
        public List<TModel> Data { get; set; }
    }
}
