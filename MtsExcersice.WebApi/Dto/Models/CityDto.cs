﻿using System.Runtime.Serialization;

namespace MtsExcersice.WebApi.Dto.Models
{
    /// <summary>
    ///     City information model
    /// </summary>
    public class CityDto
    {
        /// <summary>
        ///     Unique city identifier
        /// </summary>
        [DataMember]
        public int? Id { get; set; }

        /// <summary>
        ///     City name
        /// </summary>
        [DataMember]
        public string Name { get; set; }
    }
}
