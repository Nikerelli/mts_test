﻿using MtsTestExcersice.DataAccess.Abstractions;

namespace MtsExcersice.WebApi.Configuration
{
    /// <inheritdoc cref="IDataBaseConfiguration"/>
    public class DatabaseConfiguration : IDataBaseConfiguration
    {
        /// <inheritdoc cref="IDataBaseConfiguration.ConnectionString"/>
        public string ConnectionString { get; set; }
    }
}
