﻿using AutoMapper;
using MtsExcersice.WebApi.Dto.Models;
using MtsTestExcersice.Business.Models;

namespace MtsExcersice.WebApi.Mapping
{
    internal class CityMappingProfile : Profile
    {
        public CityMappingProfile()
        {
            CreateMap<City, CityDto>();
            CreateMap<CityDto, City>()
                .ForMember(d => d.Name, o => o.MapFrom(s => s.Name));

            CreateMap<CityFullDto, City>().ReverseMap();
        }
    }
}
