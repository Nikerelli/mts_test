﻿using AutoMapper;
using MtsExcersice.WebApi.Dto.Models;
using MtsTestExcersice.Business.Models;

namespace MtsExcersice.WebApi.Mapping
{
    internal class RestaurantMappingProfile : Profile
    {
        public RestaurantMappingProfile()
        {
            CreateMap<RestaurantDto, Restaurant>();

            CreateMap<Restaurant, RestaurantDto>()
                .ForMember(d => d.Name, o => o.MapFrom(s => s.Name)); ;
        }
    }
}
