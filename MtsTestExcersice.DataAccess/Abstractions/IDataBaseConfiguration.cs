﻿namespace MtsTestExcersice.DataAccess.Abstractions
{
    /// <summary/>
    public interface IDataBaseConfiguration
    {
        /// <summary/>
        string ConnectionString { get; set; }
    }
}
