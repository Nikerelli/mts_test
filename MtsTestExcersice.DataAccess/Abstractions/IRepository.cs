﻿using System;
using MtsTestExcersice.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MtsTestExcersice.DataAccess.Abstractions
{
    public interface IRepository
    {
        /// <summary>
        ///     Returnes requested entities with specified filtering expression
        /// </summary>
        IQueryable<TEntity> Get<TEntity>() where TEntity : class;

        /// <summary>
        ///     Adds entity to storage
        /// </summary>
        /// <returns>Entity identifier</returns>
        Task<TEntity> AddAsync<TEntity>(TEntity entity, CancellationToken token = default);


        /// <summary>
        ///     Adds entity to storage
        /// </summary>
        /// <returns>Entity identifier</returns>
        TEntity Update<TEntity>(TEntity entity);


        /// <summary>
        ///     Adds entity to storage
        /// </summary>
        /// <returns>Entity identifier</returns>
        TEntity Remove<TEntity>(TEntity entity);

        Task CommitChangedAsync();

        void RemoveRange(IEnumerable<object> entities);
    }
}
