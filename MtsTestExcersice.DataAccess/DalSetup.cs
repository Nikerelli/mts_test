﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MtsTestExcersice.DataAccess.Abstractions;

namespace MtsTestExcersice.DataAccess
{
    public static class DalSetup
    {
        public static IServiceCollection SetupDal(this IServiceCollection services)
        {
            return services.AddScoped<IRepository, MtsContext>()
                .AddDbContext<MtsContext>((p,b) => b.UseSqlServer(p.GetService<IDataBaseConfiguration>().ConnectionString));
        }
    }
}
