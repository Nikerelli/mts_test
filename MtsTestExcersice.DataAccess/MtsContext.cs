﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using MtsTestExcersice.DataAccess.Abstractions;
using MtsTestExcersice.Domain;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MtsTestExcersice.DataAccess
{
    public class MtsContext : DbContext, IRepository
    {
        public DbSet<City> Cities { get; set; }
        public DbSet<Restaurant> Restaurants { get; set; }

        public MtsContext(DbContextOptions<MtsContext> options) : base(options) {}

        /// <inheritdoc cref="IRepository.Get"/>
        IQueryable<TEntity> IRepository.Get<TEntity>() => Set<TEntity>();

        /// <inheritdoc cref="IRepository.Add"/>
        async Task<TEntity> IRepository.AddAsync<TEntity>(TEntity entity, CancellationToken token = default)
        {
            var addEntry = await base.AddAsync(entity, token);
            return (TEntity) addEntry.Entity;
        }

        TEntity IRepository.Update<TEntity>(TEntity entity)
        {
            return (TEntity) this.Update(entity).Entity;
        }

        TEntity IRepository.Remove<TEntity>(TEntity entity)
        {
            return (TEntity) this.Remove(entity).Entity;
        }


        void IRepository.RemoveRange(IEnumerable<object> entities) => RemoveRange(entities);

        async Task IRepository.CommitChangedAsync() => await SaveChangesAsync();

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CityRestaurantMappingEntity>().HasKey(cr => new {cr.RestaurantId, cr.CityId});

            modelBuilder.Entity<CityRestaurantMappingEntity>()
                .HasOne(r => r.City)
                .WithMany(p => p.Restaurants)
                .HasForeignKey(pt => pt.CityId);

            modelBuilder.Entity<CityRestaurantMappingEntity>()
                .HasOne(pt => pt.Restaurant)
                .WithMany(p => p.Cities)
                .HasForeignKey(pt => pt.RestaurantId);
        }
    }
}
