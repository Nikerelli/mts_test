﻿using AutoMapper;
using CodeJam;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MtsTestExcersice.Business.Abstractions;
using MtsTestExcersice.DataAccess.Abstractions;
using MtsTestExcersice.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using City = MtsTestExcersice.Business.Models.City;

namespace MtsTestExcersice.Business.Services
{
    internal class CityOperationService : IOperationService<City>
    {
        private readonly IMapper _mapper;
        private readonly IRepository _repository;
        private readonly ILogger<CityOperationService> _logger;

        public CityOperationService(IMapper mapper, IRepository repository, ILoggerFactory logFactory)
        {
                _mapper = mapper;
                _logger = logFactory.CreateLogger<CityOperationService>();
                _repository = repository;
        }

        public async Task<List<City>> GetAllAsync()
        {
            try
            {
                var entities = _repository.Get<Domain.City>()
                    .Include(x => x.Restaurants)
                    .ThenInclude(m => m.Restaurant)
                    .ToList();
                var models = _mapper.Map<List<City>>(entities);
                return await Task.FromResult(models);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToDiagnosticString());
                throw ex;
            }
        }

        public async Task AddAsync(City model)
        {
            try
            {
                var entity = _mapper.Map<Domain.City>(model);
                await _repository.AddAsync(entity);
                await _repository.CommitChangedAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToDiagnosticString());
                throw ex;
            }
        }

        public async Task UpdateAsync(City model)
        {
            try
            {
                var entity = _repository.Get<Domain.City>()
                    .Include(c => c.Restaurants)
                    .FirstOrDefault(x => x.Id == model.Id);

                entity = _mapper.Map(model, entity);

                entity.Restaurants = GetMappingEntities(model,entity);
                _repository.Update(entity);
                await _repository.CommitChangedAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToDiagnosticString());
                throw ex;
            }
        }

        public async Task RemoveAsync(string id)
        {
            try
            {
                if (Int32.TryParse(id, out int result))
                {
                    var entity = _repository.Get<Domain.City>()
                        .FirstOrDefault(x => x.Id == result);
                    if (entity != null)
                    {
                        _repository.Remove(entity);
                        await _repository.CommitChangedAsync();
                        return;
                    }
                }

                throw new ArgumentException("Entity identification error");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToDiagnosticString());
                throw ex;
            }
        }

        private List<CityRestaurantMappingEntity> GetMappingEntities(City model, Domain.City entity)
        {
            if ((model.Restaurants == null || !model.Restaurants.Any()) && !entity.Restaurants.Any())
            {
                return new List<CityRestaurantMappingEntity>();
            }

            var mappingEntities = entity.Restaurants
                .Where(r => model.Restaurants.Any(rm => rm.Id == r.RestaurantId) && r.CityId == model.Id)
                .ToList();
            
            _repository.RemoveRange(mappingEntities.Any() ? entity.Restaurants.Except(mappingEntities) : entity.Restaurants);

            if (mappingEntities.Count != model.Restaurants.Count)
            {
                var newRestaurantIds = model.Restaurants.Select(r => r.Id)
                    .Except(mappingEntities.Select(r => r.RestaurantId));

                var restuarants = _repository.Get<Restaurant>()
                    .ToList()
                    .Where(r => newRestaurantIds.Any(rm => rm == r.Id))
                    .Select(r => new CityRestaurantMappingEntity { CityId = model.Id, RestaurantId = r.Id });
                mappingEntities.AddRange(restuarants);
            }

            return mappingEntities.ToList();
        }
    }
}
