﻿using AutoMapper;
using CodeJam;
using Microsoft.Extensions.Logging;
using MtsTestExcersice.Business.Abstractions;
using MtsTestExcersice.Business.Models;
using MtsTestExcersice.DataAccess.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MtsTestExcersice.Business.Services
{
    internal class RestaurantOperationService :  IOperationService<Restaurant>
    {
        private readonly IMapper _mapper;
        private readonly IRepository _repository;
        private readonly ILogger<RestaurantOperationService> _logger;

        public RestaurantOperationService(IMapper mapper, IRepository repository, ILoggerFactory logFactory)
        {
            _mapper = mapper;
            _logger = logFactory.CreateLogger<RestaurantOperationService>();
            _repository = repository;
        }

        public async Task<List<Restaurant>> GetAllAsync()
        {
            try
            {
                var entities = _repository.Get<Domain.Restaurant>()
                    .Include(x => x.Cities);
                var models = _mapper.Map<List<Restaurant>>(entities);
                return await Task.FromResult(models);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToDiagnosticString());
                throw ex;
            }
        }

        public async Task AddAsync(Restaurant model)
        {
            try
            {
                var entity = _mapper.Map<Domain.Restaurant>(model);
                await _repository.AddAsync(entity);
                await _repository.CommitChangedAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToDiagnosticString());
                throw ex;
            }
        }

        public async Task UpdateAsync(Restaurant model)
        {
            try
            {
                var entity = _repository.Get<Domain.Restaurant>().FirstOrDefault(x => x.Id == model.Id);
                entity = _mapper.Map(model,entity);

                _repository.Update(entity);
                await _repository.CommitChangedAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToDiagnosticString());
                throw ex;
            }
        }

        public async Task RemoveAsync(string id)
        {
            try
            {
                if (Int32.TryParse(id, out int result))
                {
                    var entity = _repository.Get<Domain.Restaurant>().FirstOrDefault(x => x.Id == result);
                    if (entity != null)
                    {
                        _repository.Remove(entity);
                        await _repository.CommitChangedAsync();
                        return;
                    }
                }

                throw new ArgumentException("Entity identification error");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToDiagnosticString());
                throw ex;
            }
        }
    }
}
