﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using MtsTestExcersice.Business.Abstractions;
using MtsTestExcersice.Business.Mapping;
using MtsTestExcersice.Business.Models;
using MtsTestExcersice.Business.Services;
using MtsTestExcersice.DataAccess;

namespace MtsTestExcersice.Business
{
    public static class BusinessSetup
    {
        public static IServiceCollection SetupBusines(this IServiceCollection services)
        {
            return services.SetupDal()
                .AddScoped<IOperationService<City>, CityOperationService>()
                .AddScoped<IOperationService<Restaurant>, RestaurantOperationService>()
                .SetupMappingProfiles();
        }

        internal static IServiceCollection SetupMappingProfiles(this IServiceCollection services)
        {
            return services.SetupDal()
                .AddScoped<Profile, CityMappingProfile>()
                .AddScoped<Profile, RestaurantMappingProfile>();
        }

    }
}
