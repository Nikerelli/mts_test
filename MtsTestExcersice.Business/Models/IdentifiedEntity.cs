﻿namespace MtsTestExcersice.Business.Models
{
    public class IdentifiedEntity<TIdentifier>
    {
        public TIdentifier Id { get; set; }
    }
}
