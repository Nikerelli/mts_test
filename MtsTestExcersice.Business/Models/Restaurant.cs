﻿using System.Collections;
using System.Collections.Generic;

namespace MtsTestExcersice.Business.Models
{
    public class Restaurant : IdentifiedEntity<int>
    {
        public string Name { get; set; }
        public List<int> CityIds { get; set; }
    }
}
