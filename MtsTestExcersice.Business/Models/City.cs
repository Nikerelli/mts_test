﻿using System.Collections.Generic;

namespace MtsTestExcersice.Business.Models
{
    public class City : IdentifiedEntity<int>
    {
        public string Name { get; set; }
        public List<Restaurant> Restaurants { get; set; }
    }
}
