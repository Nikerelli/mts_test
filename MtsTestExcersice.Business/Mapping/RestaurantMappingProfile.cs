﻿using System.Linq;
using AutoMapper;
using MtsTestExcersice.Business.Models;

namespace MtsTestExcersice.Business.Mapping
{
    internal class RestaurantMappingProfile : Profile
    {
        public RestaurantMappingProfile()
        {
            CreateMap<Restaurant, Domain.Restaurant>().ReverseMap()
                .ForMember(d => d.CityIds, o => o.MapFrom(s => s.Cities.Select(x => x.CityId)));


            CreateMap<Domain.CityRestaurantMappingEntity, Restaurant>()
                .ForMember(d => d.Id, o => o.MapFrom(s => s.Restaurant.Id))
                .ForMember(d => d.Name, o => o.MapFrom(s => s.Restaurant.Name));
        }
    }
}
