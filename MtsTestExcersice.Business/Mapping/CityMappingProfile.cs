﻿using AutoMapper;
using MtsTestExcersice.Business.Models;
using System.Linq;

namespace MtsTestExcersice.Business.Mapping
{
    internal class CityMappingProfile : Profile
    {
        public CityMappingProfile()
        {
            CreateMap<City, Domain.City>()
                .ForMember(d => d.Id, o => o.Ignore())
                .ForMember(d => d.Restaurants, o => o.Ignore());

            CreateMap<Domain.City, City>()
                .ForMember(d => d.Restaurants, o => o.MapFrom(s => s.Restaurants.Select(r => r.Restaurant)));

            CreateMap<Domain.CityRestaurantMappingEntity, City>()
                .ForMember(d => d.Id, o => o.MapFrom(s => s.City.Id))
                .ForMember(d => d.Name, o => o.MapFrom(s => s.City.Name));
        }
    }
}
