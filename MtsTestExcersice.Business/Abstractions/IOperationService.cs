﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace MtsTestExcersice.Business.Abstractions
{
    public interface IOperationService<TModel>
        where TModel : class
    {
        Task<List<TModel>> GetAllAsync();

        Task AddAsync(TModel model);

        Task UpdateAsync(TModel model);

        Task RemoveAsync(string Id);
    }
}
